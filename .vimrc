" Setup:
" 1. Install 64-bit Vim
" 2. Copy all this to the Vimrc file
" 3. Install NeoBundle (git clone into the right directory)
" 4. Run Vim to let NeoBundle fetch all the plugins.
" 5. Build YouCompleteMe (Windows: https://github.com/Valloric/YouCompleteMe/wiki/Windows-Installation-Guide). If CMake can't find python on windows, see http://ericsilva.org/2012/10/11/restoring-your-python-registry-in-windows/
" 5.5 Nevermind, YCM just doesn't work on Windows. Try NeoComplete (https://github.com/Shougo/neocomplete.vim) isntead. Get a full-Vim version ofr windows that supports lua, and put lua53.dll/lua52.dll in the gvim.exe directory.
" 6. Install powerline fonts (try https://github.com/runsisi/consolas-font-for-powerline or https://github.com/powerline/fonts), or if that doesn't work, disable them with the 'set' command below.
" 6.5 Nevermind, actually install fully-patched fonts if you're not on Windows...see the devicons-plugin below for a URL.
" 7. Install Exuberant Ctags (on windows, just extract all the files somewhere and add it to your PATH)
"
" Finally, restart Vim again to let everything take effect.

if &compatible
    set nocompatible
endif

set runtimepath^=~/.vim/bundle/neobundle.vim/

call neobundle#begin(expand('~/.vim/bundle/'))

NeoBundleFetch 'Shougo/neobundle.vim'

"Insert bundles here:

" Add the nice statusline at top/bottom
NeoBundle 'bling/vim-airline' 

" Themes for Airline.
NeoBundle 'vim-airline/vim-airline-themes' 

" Show Git status of each line in gutter
NeoBundle 'airblade/vim-gitgutter'

" File browser (toggle with F9)
NeoBundle 'scrooloose/nerdtree'

" Show git status of files in NerdTree
NeoBundle 'Xuyuanp/nerdtree-git-plugin'

" NeoBundle 'tpope/vim-fugitive' Not using this ATM, revisit

" Generic syntax-checker that runs on save
NeoBundle 'scrooloose/syntastic' 

" Autocomplete smartly
"NeoBundle 'valloric/youcompleteme'

" Syntax for Jade/pug
NeoBundle 'digitaltoad/vim-jade'

" Show line indents visually
NeoBundle 'yggdroot/indentline'

" Commenting shortcuts for all languages
NeoBundle 'scrooloose/nerdcommenter'

" PHP Stuff
NeoBundle 'shawncplus/phpcomplete.vim'
NeoBundle 'stanangeloff/php.vim'
NeoBundle '2072/php-indenting-for-vim'

" Neocomplete stuff: only for windows ====
NeoBundle 'shougo/neocomplete.vim'
NeoBundle 'Shougo/neosnippet'
NeoBundle 'Shougo/neosnippet-snippets'
" ========================================

" Tagbar
NeoBundle 'xolox/vim-easytags'
NeoBundle 'majutsushi/tagbar'

" Extra plugins for Xolox stuff
NeoBundle 'xolox/vim-shell'
NeoBundle 'xolox/vim-misc'

" Preview colors of color-codes in HTML/CSS files
NeoBundle 'gorodinskiy/vim-coloresque'

" Fancy Start screen
NeoBundle 'mhinz/vim-startify'

NeoBundle 'raimondi/delimitmate'

" Sublime-like multicursors (use <C-n>)
" NeoBundle 'terryma/vim-multiple-cursors'   Let's try to get rid of this and use native features instead.

" Hotkeys to access surrounding-chars
NeoBundle 'tpope/vim-surround'

" Sublime-like filename-finder
NeoBundle 'ctrlpvim/ctrlp.vim'

" Improved Windows functionality -- only needed in Windows.
"NeoBundle 'kkoenig/wimproved.vim'  

" Only show cursorline highlight in the active window
NeoBundle 'vim-scripts/CursorLineCurrentWindow'

" Solarized theme for GVim
"NeoBundle 'altercation/vim-colors-solarized'

" Python syntax, linting
NeoBundle 'klen/python-mode'

" Highlight trailing whitespace, delete with ctrl-backspace
NeoBundle 'bronson/vim-trailing-whitespace'

" Modern JS syntax
NeoBundle 'othree/yajs.vim' " Try this out...see if performance is acceptable

" For f/F/t/T, highlight userful jump-points
NeoBundle 'unblevable/quick-scope'

" Swap two windows by doing \ww in the source, then \ww in the dest
NeoBundle 'wesQ3/vim-windowswap' 

" Allow dates to be incremented with <C-A>/<C-X>
NeoBundle 'tpope/vim-speeddating'

" Enable .-repeating of tpope plugin commands
NeoBundle 'tpope/vim-repeat'

" Allow args in brackets to be expanded/compressed with a single command
NeoBundle 'FooSoft/vim-argwrap'

" Easier grepping...use "gs" to initiate grep, tab to switch grep-tools.
" Results end up in the quickfix list.
NeoBundle 'mhinz/vim-grepper'

" Gotham colorscheme
NeoBundle 'whatyouhide/vim-gotham'

" Typescript syntax
NeoBundle 'leafgarland/typescript-vim'

" Load this after everything else to get icons right. Don't forget to grab
NeoBundle 'ryanoasis/vim-devicons'
" patched fonts from https://github.com/ryanoasis/nerd-fonts

call neobundle#end()

filetype plugin indent on

NeoBundleCheck



" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
    finish
endif

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

if has("vms")
    set nobackup		" do not keep a backup file, use versions instead
else
    set backup		" keep a backup file (restore to previous version)
    set undofile		" keep an undo file (undo changes after closing)
endif
set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching

"
" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
    set mouse=a
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
    syntax on
    set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")
    " Enable file type detection.
    " Use the default filetype settings, so that mail gets 'tw' set to 72,
    " 'cindent' is on in C files, etc.
    " Also load indent files, to automatically do language-dependent indenting.
    filetype plugin indent on

    " Put these in an autocmd group, so that we can delete them easily.
    augroup vimrcEx
        au!

        " For all text files set 'textwidth' to 78 characters.
        autocmd FileType text setlocal textwidth=78

        " When editing a file, always jump to the last known cursor position.
        " Don't do it when the position is invalid or when inside an event handler
        " (happens when dropping a file on gvim).
        autocmd BufReadPost *
                    \ if line("'\"") >= 1 && line("'\"") <= line("$") |
                    \   exe "normal! g`\"" |
                    \ endif
    augroup END
else
    set autoindent		" always set autoindenting on
endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
    command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
                \ | wincmd p | diffthis
endif

if has('langmap') && exists('+langnoremap')
    " Prevent that the langmap option applies to characters that result from a
    " mapping.  If unset (default), this may break plugins (but it's backward
    " compatible).
    set langnoremap
endif

" Show line numbers.
set number
"set relativenumber
if has('autocmd')
augroup vimrc_linenumbering
    " Set line numbers to relative for the current window only
    autocmd!
    autocmd WinLeave *
        \ if &number |
        \   set norelativenumber |
        \ endif
    autocmd BufWinEnter *
        \ if &number |
        \   set relativenumber |
        \ endif
    autocmd VimEnter *
        \ if &number |
        \   set relativenumber |
        \ endif
augroup END
endif


" Enhance search by highlighting
set incsearch
set hlsearch

" all-lowercase searches are insensitive, mixed-case searches are case
" sensitive
set smartcase
set ignorecase

"Open files to same position as last time
augroup resCur
    autocmd!
    autocmd BufReadPost * call setpos(".", getpos("'\""))
augroup END

" Enable fancy fonts
let g:airline_powerline_fonts = 1

" Startup airline plugin automatically (why does this work?)
set laststatus=2

" Shortcut "w!!" for forcing vim to write with sudo permissions
cmap w!! w !sudo tee %

" Syntastic plugin configs
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:ycm_global_ycm_extra_conf = '~/Projects/loc8r/'
let g:ycm_confirm_extra_conf = 0

" Set indentation to 4-spaces
set tabstop=4 expandtab shiftwidth=4 smarttab

" Indentation Exceptions:
autocmd FileType javascript setlocal noexpandtab  " Leave JS tabs alone

" Set Line Number gutter background colour to black
hi LineNr ctermbg=black

" Push \\ to stop highlighting after a search.
nnoremap <leader><leader> :noh<return><leader><leader>

" Folding settings
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=1

" Use Enter key to insert blank lines below cursor
nnoremap <Enter> o<esc>

" Use spacebar to fold local regions
nnoremap <space> za

" Set color of indentation-showing character
let g:indentLine_color_term = 236

" Set nerdcommter-plugin shortcut
map <C-_> :NERDComToggleComment

" Highlight the 101st column of wide lines
highlight ColorColumn ctermbg=magenta
call matchadd('ColorColumn', '\%101v', 100)

"=====[ Highlight matches when jumping to next ]=============
" This rewires n and N to do the highlighing...
nnoremap <silent> n   n:call HLNext(0.4)<cr>
nnoremap <silent> N   N:call HLNext(0.4)<cr>
" Blink the line containing the match...
function! HLNext (blinktime)
    set invcursorline
    redraw
    exec 'sleep ' . float2nr(a:blinktime * 1000) . 'm'
    set invcursorline
    redraw
endfunction

" Always give a couple lines of breathing room around cursor when
" scrolling
set scrolloff=7
set sidescrolloff=5

" Override the colour used to for highlighting search matches (this one is
" actually readable, yay)
hi Search ctermbg=21

" Override colour for matching brackets
hi MatchParen ctermbg=21

if has("gui_running")
    if has("gui_gtk2")
        set guifont=Inconsolata\ 12
    elseif has("gui_macvim")
        set guifont=Menlo\ Regular:h14
    elseif has("gui_win32")
        set guifont=Hack:h11:cANSI
    endif

    set background=dark
    colorscheme gotham

    :set guioptions-=m  "remove menu bar
    :set guioptions-=T  "remove toolbar
    :set guioptions-=r  "remove right-hand scroll bar
    :set guioptions-=L  "remove left-hand scroll bar
endif

if has('gui_win32')
    " If window doesn't have nice symbols, we can improvise...
    "let g:NERDTreeIndicatorMapCustom = {
    " \ "Modified"  : "~",
    "\ "Staged"    : "+",
    "\ "Untracked" : "*",
    "\ "Renamed"   : ">",
    "\ "Unmerged"  : "=",
    "\ "Deleted"   : "X",
    "\ "Dirty"     : "x",
    "\ "Clean"     : ":)",
    "\ "Unknown"   : "?"
    "\ }
endif

" Neocomplete settings for windows =============
let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_smart_case = 1
let g:neocomplete#sources#syntax#min_keyword_length = 3

" Fixes to make Neocomplete compatible with multi-cursor plugin
" Called once right before you start selecting multiple cursors
"function! Multiple_cursors_before()
"    if exists(':NeoCompleteLock')==2
"        exe 'NeoCompleteLock'
"    endif
"endfunction
"
"Called once only when the multiple selection is canceled
" (default <Esc>)
"function! Multiple_cursors_after()
"    if exists(':NeoCompleteUnlock')==2
"        exe 'NeoCompleteUnlock'
"    endif
"endfunction

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
" Close popup by <C-Space>.
inoremap <expr><C-Space> pumvisible() ? "\<C-y>" : "\<Space>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
" =========== Neocomplete settings

" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
"imap <expr><TAB>
" \ pumvisible() ? "\<C-n>" :
" \ neosnippet#expandable_or_jumpable() ?
" \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
            \ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For conceal markers.
if has('conceal')
    set conceallevel=2 concealcursor=niv
endif

" Use ESLint for JS syntastic-plugin chcecking. Make sure the .eslintrc is configured!
let g:syntastic_javascript_checkers = ['eslint']

" Enable UTF-8. Required for using fancy fonts.
set encoding=utf8

" Use F8 to toggle the tagbar display
nmap <F8> :TagbarToggle<CR>

" Ctrl P: limit the number of files to index
let g:ctrlp_max_files = 30000

" Go to next/prev buffers with gn/gp
nmap gn :bn<CR>
nmap gp :bp<CR>
nmap gd :bd<CR>

" Toggle NerdTree visibility with F9
nnoremap <F9> :NERDTreeToggle<CR>

" Let Airline plugin show the buffers at the top of the window
let g:airline#extensions#tabline#enabled = 1

" Set theme for Airline
let g:airline_theme='jellybeans'

" Enable Wimproved plugin stuff
autocmd GUIEnter * silent! WToggleClean

" Highlight the current line the cursor is on
set cursorline

" Override the fullscreen button (F11) when on Windows with a fixed version.
" This made need to go in the .gvimrc file instead.
if has("gui_win32")
    nnoremap <F11> :WToggleFullscreen
endif

" Move faster between screen splits.
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Open new splits to the right and bottom
set splitbelow
set splitright

" ============ Python-mode =============
" Activate rope
" Keys:
" K             Show python docs
" <Ctrl-Space>  Rope autocomplete
" <Ctrl-c>g     Rope goto definition
" <Ctrl-c>d     Rope show documentation
" <Ctrl-c>f     Rope find occurrences
" <Leader>b     Set, unset breakpoint (g:pymode_breakpoint enabled)
" [[            Jump on previous class or function (normal, visual, operator modes)
" ]]            Jump on next class or function (normal, visual, operator modes)
" [M            Jump on previous class or method (normal, visual, operator modes)
" ]M            Jump on next class or method (normal, visual, operator modes)
let g:pymode_rope = 1    " Might wanna turn this off, its kinda annoying?

" Documentation
let g:pymode_doc = 1
let g:pymode_doc_key = 'K'

"Linting
let g:pymode_lint = 1
let g:pymode_lint_checker = "pyflakes,pep8"
" Auto check on save
let g:pymode_lint_write = 1

" Support virtualenv
let g:pymode_virtualenv = 1

" Enable breakpoints plugin
let g:pymode_breakpoint = 1
let g:pymode_breakpoint_bind = '<leader>b'

" syntax highlighting
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all

" Don't autofold code
let g:pymode_folding = 0
" ============ Python-mode =============

" Let vim buffer redraws
set lazyredraw

" === Move cursor line (or vmode selection) up or down with the arrow keys ===
function! s:SetUndojoinFlag(mode)
    augroup undojoin_flag
        autocmd!
        if a:mode ==# 'v'
            autocmd CursorMoved * autocmd undojoin_flag CursorMoved * autocmd! undojoin_flag
        else
            autocmd CursorMoved * autocmd! undojoin_flag
        endif
    augroup END
endfunction

function! s:Undojoin()
    if exists('#undojoin_flag#CursorMoved')
        silent! undojoin
    endif
endfunction

nnoremap <silent> <Down> :<C-u>call <SID>Undojoin()<CR>:<C-u>move +1<CR>==:<C-u>call <SID>SetUndojoinFlag('n')<CR>
nnoremap <silent> <Up>   :<C-u>call <SID>Undojoin()<CR>:<C-u>move -2<CR>==:<C-u>call <SID>SetUndojoinFlag('n')<CR>
xnoremap <silent> <Down> :<C-u>call <SID>Undojoin()<CR>:<C-u>'<,'>move '>+1<CR>gv=:<C-u>call <SID>SetUndojoinFlag('v')<CR>gv
xnoremap <silent> <Up>   :<C-u>call <SID>Undojoin()<CR>:<C-u>'<,'>move '<-2<CR>gv=:<C-u>call <SID>SetUndojoinFlag('v')<CR>gv
" === End Move cursor line stuff ===

" Toggle friendly paste mode with F10 (push this before and after pasting into Vim)
set pastetoggle=<F10>

" Do not store vimrc values and code folding in saved sessions.
set ssop-=options
set ssop-=folds

" Push F3 to insert a timestamp
nmap <F3> i<C-R>=strftime("[%Y-%m-%d %a %I:%M %p]")<CR><Esc>
imap <F3> <C-R>=strftime("[%Y-%m-%d %a %I:%M %p]")<CR>

" Trigger a highlight (from quick-scope plugin) in the appropriate direction when pressing these keys:
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

" Exit insert mode with k-j sequence (must be pushed quickly)
imap kj <Esc>

" Set CtrlP to start searching in PWD
let g:ctrlp_working_path_mode = 'w'

" Scroll horizontally one char at a time
set sidescroll=1

" Start files folded by default
set foldlevelstart=1

" Let CtrlP ignore stuff (use '|' to concat, but escape it, like '\|')
let g:ctrlp_custom_ignore = {
    \ 'dir': 'node_modules$\|cache$\|log$\|templates_c$\|.ropeproject$',
    \ 'file': '\.log$\|\.png$\|\.jpg$\|\.bak$'
    \ }

set wildignore+=*.sln,*.so,*.swf,*.as,*.xls,*.meta,*.pyc

" Collapse/Expand arg lists with \a
nnoremap <silent> <leader>a :ArgWrap<CR>

" Let j/k move up/down through virtual lines as well as real lines
noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')

" Make Y yank everything from the cursor to the end of the line. This makes Y
" act more like C or D because by default, Y yanks the current line (i.e. the
" same as yy).
noremap Y y$

" Make Ctrl-e jump to the end of the current line in the insert mode. This is
" handy when you are in the middle of a line and would like to go to its end
" without switching to the normal mode.
inoremap <C-e> <C-o>$

" Use "gs" to initiate the grepper. a motion beforehand will dictate what is
" searched for (i.e. gsW to search for current WORD).
"nmap gs <plug>(GrepperOperator)
"xmap gs <plug>(GrepperOperator)
" Use \g to goto the current word in the grepper, and \f to find (with a prompt)
nnoremap <leader>g :Grepper -tool ack -cword -open -switch<CR>
nnoremap <leader>f :Grepper -tool ack -open -switch<CR>

" Linux GVim fix
set guiheadroom=0

" Let the Startify plugin automatically update sessions
let g:startify_session_persistence = 1

" Clean up buffers before loading a session
let g:startify_session_delete_buffers = 1

" Let easytags attempt to generate tag files asynchronously.
let g:easytags_async = 1

" Experimental: close HTML tags automatically.
inoremap <buffer> </ </<C-x><C-o>

" Allow macros to be executed on every line in a visual selection
xnoremap @ :<C-u>call ExecuteMacroOverVisualRange()<CR>
function! ExecuteMacroOverVisualRange()
  echo "@".getcmdline()
  execute ":'<,'>normal @".nr2char(getchar())
endfunction

" Select what you just pasted (must use right after you paste it)
nnoremap <expr> gV    "`[".getregtype(v:register)[0]."`]"

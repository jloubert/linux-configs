// gulpfile.js
//
// Instructions for how Gulp should build stuff in this project. You should be able to just run
// "gulp" in this directory and it will handle everything for you.

const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const uglify = require('gulp-uglify');

gulp.task('default', ['build-js', 'build-css'], () => {});

gulp.task('build-js', () => gulp.src('js/src/*.js')
		  .pipe(sourcemaps.init())
		  .pipe(babel())
		  .pipe(concat('machine-info.js'))
		  .pipe(uglify())
		  .pipe(sourcemaps.write('.'))
		  .pipe(gulp.dest('js/'))
);

gulp.task('build-css', () => gulp.src('style/scss/*.scss')
		  .pipe(sourcemaps.init())
		  .pipe(sass().on('error', sass.logError))
		  .pipe(cssnano())
		  .pipe(concat('index.css'))
		  .pipe(sourcemaps.write('.'))
		  .pipe(gulp.dest('style/'))
);


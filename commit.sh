#! /bin/zsh

if [ $# -eq 0 ]; then
    echo "No commit message supplied"
    exit 1
fi

RED=`tput setaf 1`
GREEN=`tput setaf 2`
RESET=`tput sgr0`

PATHS=("$HOME/" "$HOME/.config/i3/")

echo "Pulling dotfiles and committing..."

for FILE in $(git ls-tree -r master --name-only)
do
    FOUND_FILE=false

    for FILE_PATH in "${PATHS[@]}"
    do
        FILE_NAME=$FILE_PATH$FILE
        if [ -f "$FILE_NAME" ]; then
            echo "${GREEN}$(cp -v $FILE_NAME .)${RESET}"
            FOUND_FILE=true
            break
        fi
    done

    if [ $FOUND_FILE = false ]; then
        echo "${RED}$FILE not found.${RESET}"
    fi
done

git add .
git commit -m $1
git push origin master


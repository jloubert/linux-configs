# Dotfiles Repo Improvement

After seeing some cool stuff here: <https://github.com/Parth/dotfiles>, I
figured it was time to improve my own dotfiles repo.

## Key Ideas

* Keep all the main files in a `~/dotfiles/` folder so they can be kept totally
  in-sync with the repo, instead of transferring updates manually
* In the home dir, have files that simply source the main files in
  `~/dotfiles/` so those configs are brought in. In addition, these files give
  the opportunity to put in machine-specific settings without committing them
* Create a script to deploy things nicely on new machines

## TO-DO List

- [X] Tmux update
    - [X] Separate Tmux configs into main file and template
    - [X] Set up auto-deploy
    - [X] Tweak the config itself
- [-] ZSH update
    - [X] Separate ZSH configs into main file and template
    - [X] Set up auto-deploy
    - [ ] Tweak the config itself
- [ ] Vim update
    - [ ] Separate configs into main file and template
    - [ ] Set up auto-deploy
    - [ ] Tweak the config itself
- [ ] Emacs update
    - [ ] Separate Spacemacs configs into main file and template? Will this
          even work with Spacemacs?
    - [ ] Set up auto-deploy
    - [ ] Tweak the config itself
- [-] Add more stuff to deploy script
    - [X] Fix the newlines in all the files. Git is mangling them
        - I have no idea how to do this nicely. Seems so random, but brute-forcing all files in the home dir is bad...
    - [X] Pull oh-my-zsh
        - This starts up a new nested ZSH shell as its final step, which is
          annoying. I could avoid this by rewriting their install script and
          not doing that step I guess, or just C-d out of the shell
          manually.
    - [X] Pull zsh-syntax highlighting
    - [X] Change default shell to ZSH (`chsh -s /usr/bin/zsh`)
    - [ ] 256 color mode?


;; -*- mode: emacs-lisp -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

;; Installing Iosevka font
;;
;; MaxOS
;; Use this: https://github.com/robertgzr/homebrew-tap
;; So do `brew tap robertgzr/homebrew-tap`
;; Then install it with these params:
;; brew install iosevka --with-a-doublestorey --with-asterisk-low --with-brace-straight --with-caret-low --with-g-singlestorey --with-i-zshaped --with-ligset-coq --with-one-serifed --with-tilde-low --with-zero-slashed --with-percent-dots --with-underscore-low --with-l-zshaped
;; Then copy the file like it tells you.
;;
;; To tweak the build and reinstall:
;; `rm -rf` the folders involved above
;; `brew uninstall iosevka`
;; `sudo atsutil databases -remove`
;; Log out and in? Not sure if necessary here.
;; Run the install command again with adjusted params
;; Log out and in if its not showing up

;; Things to do after initial install of emacs + spacemacs:
;;
;; 0) Linux/MacOS Packages: ranger htop ispell fzf zsh ripgrep tmux nvm mlocate
;; 1) npm install -g typescript tern js-beautify eslint tslint typescript-formatter eslint-config-google
;;    To make the ESLint config extend the rulesets, you may have to manually specify the path, ex: eslint-config-google/index.js in the "extends" declaration (instead of just "google")
;; 2) $ git clone git://github.com/clausreinke/typescript-tools.git
;;    $ cd typescript-tools/
;;    $ npm install -g
;; 3) (Windows) Install "aspell" from http://aspell.net/win32/ and add it to PATH.
;; 4) (Windows) Install "MacType" from https://www.reddit.com/r/software/comments/3g2dpq/does_anyone_have_the_mactype_installer_by_any/
;;    and then run the program to configure it. Then restart the machine.


(defun dotspacemacs/layers ()
  "Configuration Layers declaration.
You should not put any user code in this function besides modifying the variable
values."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs
   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused
   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t
   ;; If non-nil layers with lazy install support are lazy installed.
   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()
   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '(
     ;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press <SPC f e R> (Vim style) or
     ;; <M-m f e R> (Emacs style) to install them.
     ;; ----------------------------------------------------------------
     ;;helm
     ivy ;; Let's try Ivy for a while.
     (auto-completion :variables
                      auto-completion-enable-sort-by-usage t
                      auto-completion-enable-help-tooltip t
                      auto-completion-enable-snippets-in-popup t)
     better-defaults
     emacs-lisp
     git
     ;osx
     ;php
     ;; (ruby :variables
     ;;       ruby-version-manager 'rvm
     ;;       ruby-enable-enh-ruby-mode t)
     markdown
     (org :variables
          org-enable-bootstrap-support t)
     (shell :variables
            shell-default-height 30
            shell-default-position 'bottom)
     ;; spell-checking
     syntax-checking
     ;; The git-gutter features from this layer were causing some slowdown, and
     ;; I don't really use the features here anyway. If I want to consider
     ;; re-enabling this, try with the variables below to disable the gutter
     ;; stuff.
     ;; (version-control :variables
     ;;                  version-control-global-margin nil
     ;;                  version-control-diff-tool 'diff-hl)
     themes-megapack
     ;lsp
     (javascript :variables
                 ;javascript-backend 'lsp ; This requires some setup/installs. See the js layer docs. Or use Tern
                 javascript-disable-tern-port-files nil)
                 ;tern-command '("C:/Users/jonathan.loubert/AppData/Roaming/npm/tern.cmd")) ;; Use this for Windows
     html
     semantic
     python
     ;django
     ;ansible
     ;; (typescript :variables
     ;;             typescript-use-tslint t
     ;;             typescript-fmt-on-save nil)
     colors
     ;sql
     theming
     ;; clojure
     ;search-engine
     (ranger :variables
             ranger-show-preview t
             ranger-show-dotfiles t
             ranger-parent-depth 2
             ranger-ignored-extensions '("mkv" "iso" "mp4" "pyc"))
     ;finance
     ;slack
     emoji
     )
   ;; List of additional packages that will be installed without being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages, then consider creating a layer. You can also put the
   ;; configuration in `dotspacemacs/user-config'.
   dotspacemacs-additional-packages '(slime
                                      nord-theme
                                      ;helm-org-rifle
                                      ;wttrin
                                      ;magit-todos
                                      exec-path-from-shell
                                      beacon
                                      frog-jump-buffer
                                      gntp)
   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()
   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages '()
   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and uninstall any
   ;; unused packages as well as their unused dependencies.
   ;; `used-but-keep-unused' installs only the used packages but won't uninstall
   ;; them if they become unused. `all' installs *all* packages supported by
   ;; Spacemacs and never uninstall them. (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration.
You should not put any user code in there besides modifying the variable
values."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil then enable support for the portable dumper. You'll need
   ;; to compile Emacs 27 from source following the instructions in file
   ;; EXPERIMENTAL.org at to root of the git repository.
   ;; (default nil)
   dotspacemacs-enable-emacs-pdumper nil

   ;; File path pointing to emacs 27.1 executable compiled with support
   ;; for the portable dumper (this is currently the branch pdumper).
   ;; (default "emacs-27.0.50")
   dotspacemacs-emacs-pdumper-executable-file "emacs-27.0.50"

   ;; Name of the Spacemacs dump file. This is the file will be created by the
   ;; portable dumper in the cache directory under dumps sub-directory.
   ;; To load it when starting Emacs add the parameter `--dump-file'
   ;; when invoking Emacs 27.1 executable on the command line, for instance:
   ;;   ./emacs --dump-file=~/.emacs.d/.cache/dumps/spacemacs.pdmp
   ;; (default spacemacs.pdmp)
   dotspacemacs-emacs-dumper-dump-file "spacemacs.pdmp"

   ;; If non-nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t
   ;; Maximum allowed time in seconds to contact an ELPA repository.
   dotspacemacs-elpa-timeout 5

   ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
   ;; This is an advanced option and should not be changed unless you suspect
   ;; performance issues due to garbage collection operations.
   ;; (default '(100000000 0.1))
   dotspacemacs-gc-cons '(100000000 0.1)

   ;; If non-nil then Spacelpa repository is the primary source to install
   ;; a locked version of packages. If nil then Spacemacs will install the
   ;; latest version of packages from MELPA. (default nil)
   dotspacemacs-use-spacelpa nil

   ;; If non-nil then verify the signature for downloaded Spacelpa archives.
   ;; (default nil)
   dotspacemacs-verify-spacelpa-archives nil

   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil
   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'.
   dotspacemacs-elpa-subdirectory nil
   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'vim
   ;; If non-nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading nil
   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'official
   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `bookmarks' `projects' `agenda' `todos'.
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 7))

   ;; True if the home buffer should respond to resize events. (default t)
   dotspacemacs-startup-buffer-responsive t

   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'text-mode

   ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
   ;; (default nil)
   dotspacemacs-initial-scratch-message nil

   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press `SPC T n' to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(nord
                         majapahit-dark
                         majapahit-light)
   ;; If non nil the cursor color matches the state color in GUI Emacs.
   dotspacemacs-colorize-cursor-according-to-state t
   ;; Default font, or prioritized list of fonts. `powerline-scale' allows to
   ;; quickly tweak the mode-line size to make separators look not too crappy.
   dotspacemacs-default-font '("Source Code Pro"
                               :size 15
                               :weight normal
                               :width normal
                               :powerline-scale 1.0)
   ;; The leader key
   dotspacemacs-leader-key "SPC"

   ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"

   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"

   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"

   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","

   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m")
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"

   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
   ;; Setting it to a non-nil value, allows for separate commands under `C-i'
   ;; and TAB or `C-m' and `RET'.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab t
   ;; If non nil `Y' is remapped to `y$' in Evil states. (default nil)
   dotspacemacs-remap-Y-to-y$ t
   ;; If non-nil, the shift mappings `<' and `>' retain visual state if used
   ;; there. (default t)
   dotspacemacs-retain-visual-state-on-shift t
   ;; If non-nil, J and K move lines up and down when in visual mode.
   ;; (default nil)
   dotspacemacs-visual-line-move-text nil
   ;; If non-nil, inverse the meaning of `g' in `:substitute' Evil ex-command.
   ;; (default nil)
   dotspacemacs-ex-substitute-global nil
   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"

   ;; If non-nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil

   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts t

   ;; If non-nil, auto-generate layout name when creating new layouts. Only has
   ;; effect when using the "jump to layout by number" commands. (default nil)
   dotspacemacs-auto-generate-layout-names nil

   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1

   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache

   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5
   ;; If non-nil, `helm' will try to minimize the space it uses. (default nil)
   dotspacemacs-helm-resize nil
   ;; if non-nil, the helm header is hidden when there is only one source.
   ;; (default nil)
   dotspacemacs-helm-no-header nil
   ;; define the position to display `helm', options are `bottom', `top',
   ;; `left', or `right'. (default 'bottom)
   dotspacemacs-helm-position 'bottom
   ;; Controls fuzzy matching in helm. If set to `always', force fuzzy matching
   ;; in all non-asynchronous sources. If set to `source', preserve individual
   ;; source settings. Else, disable fuzzy matching in all sources.
   ;; (default 'always)
   dotspacemacs-helm-use-fuzzy 'always
   ;; If non-nil the paste micro-state is enabled. When enabled pressing `p`
   ;; several times cycle between the kill ring content. (default nil)
   dotspacemacs-enable-paste-transient-state t
   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4

   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom

   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil

   ;; If non-nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar nil
   ;; If non nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil

   ;; If non-nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil

   ;; If non-nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90

   ;; If non-nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t

   ;; If non-nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t

   ;; If non-nil unicode symbols are displayed in the mode line.
   ;; If you use Emacs as a daemon and wants unicode characters only in GUI set
   ;; the value to quoted `display-graphic-p'. (default t)
   dotspacemacs-mode-line-unicode-symbols t

   ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t

   ;; Control line numbers activation.
   ;; If set to `t' or `relative' line numbers are turned on in all `prog-mode' and
   ;; `text-mode' derivatives. If set to `relative', line numbers are relative.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; (default nil)
   dotspacemacs-line-numbers nil

   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil

   ;; If non-nil `smartparens-strict-mode' will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil

   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc�
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis t
   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all

   ;; If non-nil, start an Emacs server if one is not already running.
   ;; (default nil)
   dotspacemacs-enable-server nil

   ;; Set the emacs server socket location.
   ;; If nil, uses whatever the Emacs default is, otherwise a directory path
   ;; like \"~/.emacs.d/server\". It has no effect if
   ;; `dotspacemacs-enable-server' is nil.
   ;; (default nil)
   dotspacemacs-server-socket-dir nil

   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil

   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")

   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"

   ;; Format specification for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil

   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'trailing

   ;; Either nil or a number of seconds. If non-nil zone out after the specified
   ;; number of seconds. (default nil)
   dotspacemacs-zone-out-when-idle nil

   ;; temp
   dotspacemacs-mode-line-theme 'spacemacs

   ;; Run `spacemacs/prettify-org-buffer' when
   ;; visiting README.org files of Spacemacs.
   ;; (default nil)
   dotspacemacs-pretty-docs nil))

(defun dotspacemacs/user-init ()
  "Initialization for user code:
This function is called immediately after `dotspacemacs/init', before layer
configuration.
It is mostly for variables that should be set before packages are loaded.
If you are unsure, try setting them in `dotspacemacs/user-config' first."
  )

(defun dotspacemacs/user-load ()
  "Library to load while dumping.
This function is called while dumping Spacemacs configuration. You can
`require' or `load' the libraries of your choice that will be included
in the dump."
  )

(defun dotspacemacs/user-config ()
  "Configuration function for user code.
This function is called at the very end of Spacemacs initialization after
layers configuration.
This is the place where most of your configurations should be done. Unless it is
explicitly specified that a variable should be set before a package is loaded,
you should place your code here."

  (setq-default evil-escape-key-sequence "kj")
  
  (define-key evil-normal-state-map "\\" 'evil-repeat-find-char-reverse)
  
  ;; In text buffers, wrap long lines
  (add-hook 'text-mode-hook 'auto-fill-mode)
  
  ;; Win 7 fix
  (setq w32-get-true-file-attributes nil)
  
  ;;;;;;;;;;;;;
  ;; Org-mode config
  (setq my/main-org-folder "c:/Users/mail/Dropbox/org") ; Update this for new machines
  (setq org-agenda-files (list my/main-org-folder))
  (setq org-todo-keywords
        '((sequence "TODO(t)" "UNDERWAY(u)" "BLOCKED(b)" "|" "DONE(d)" "CANCELLED(c)")))
  (setq org-enforce-todo-dependencies t)
  (setq org-enforce-todo-checkbox-dependencies t)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)
     (clojure . t)
     (shell . t)))
  (setq markdown-command "pandoc")
  (setq org-default-notes-file (concat my/main-org-folder "/captured-and-unfiled.org"))
  (setq org-refile-targets (quote ((org-agenda-files :maxlevel . 3))))
  (setq org-reverse-note-order nil)  ; Send re-filed TODOs to the bottom
  (setq org-log-into-drawer t)
  (setq org-log-reschedule t)
  (setq org-log-redeadline t)
  (setq org-capture-templates
   (backquote
    (("t" "TODO Task"
      entry
      (file+headline ,(concat my/main-org-folder "/captured-and-unfiled.org") "Tasks")
       ,(concat "* TODO %^{Task Title}\n"
               "  DEADLINE: %^{Deadline}t\n"
               "  :LINKS:\n"
               "  :END:\n"
               "  %U\n"
               "  %?")
       :empty-lines-after 1 :clock-in t :clock-resume t)
     ("b" "Bug Fix"
      entry
      (file+headline ,(concat my/main-org-folder "/todos.org") "Blinkk generic TODOs")
       ,(concat "* TODO %^{Task Title}\n"
                 "  DEADLINE: %^{Deadline}t\n"
                 "  :LINKS:\n"
                 "  :END:\n"
                 "  %U\n"
                 "\n"
                 "  - [ ] Reproduce it\n"
                 "  - [ ] Determine the root cause\n"
                 "  - [ ] Fix it\n"
                 "  - [ ] Stage the fix and update the ticket\n"
                 "\n"
                 "  %?")
       :empty-lines-after 1 :clock-in t :clock-resume t)
     ("d" "Developer Note"
      entry
      (file+headline ,(concat my/main-org-folder "/developer.org") "2019")
      ,(concat
        "* %^{Note Title} %^g\n"
        "  %U\n"
        "  %?")
      :empty-lines-after 1)
     ("j" "Journal Entry"
      entry
      (file+olp+datetree ,(concat my/main-org-folder"/journal.org"))
      ,(concat
        "* %^{Note Title} %^g\n"
        "  %U\n"
        "  %?")
      :empty-lines-after 1)
     ("m" "DND SKT Monster stats"
      entry
      (file ,(concat my/main-org-folder "/dnd-skt/monsters.org"))
      ,(concat "* %^{Monster Name}\n"
               "  | HP | AC | Size | Speed | STR | DEX | CON | INT | WIS | CHA |\n"
               "  |------------------------------------------------------------|%?\n"
               "** Other Attributes\n"
               "   - \n"
               "** Actions\n"
               "   | Name | Acc | Rng | Dmg | Typ | Etc |\n"
               "   |------------------------------------|\n"
               "** Spells\n"
               "   - Slots:\n"
               "   - Spells known:\n"
               "     | Name | Save/Attack | Ref | Effect |\n"
               "     |-----------------------------------|\n")
      :empty-lines-after 1)
     ("e" "Event"
      entry
      (file+headline ,(concat my/main-org-folder "/captured-and-unfiled.org") "Events")
      "* %^{Event Title}\n  %^{Event Date/Time}T\n  %?"
      :prepend t :empty-lines-after 1))))
  (setq org-hide-emphasis-markers t)
  (setq org-use-fast-todo-selection t)
  (spacemacs/set-leader-keys-for-major-mode 'org-mode "/" 'helm-org-rifle-agenda-files)
  (spacemacs/set-leader-keys-for-major-mode 'org-mode "g" 'helm-org-in-buffer-headings)
  ;;;;;;;;;;;;;
  
  ;; Get SLIME to use the right version of Lisp
  (setq inferior-lisp-program "sbcl")
  
  ;; Fancy symbols in Clojure mode
  (setq clojure-enable-fancify-symbols nil)
  
  ;; Add more search engines to the search-engine layer
  ;; (push '(mdn
  ;;         :name "Mozilla Developer Network"
  ;;         :url "https://developer.mozilla.org/en-US/search?q=%s")
  ;;       search-engine-alist)
  ;; Turn on Company mode everywhere
  (global-company-mode)
  
  ;; Flycheck config
  (setq flycheck-disabled-checkers '(javascript-jshint))
  ;(setq flycheck-coffeelintrc "coffeelint.json")
  
  ;; Show timestamps in undo tree
  (setq undo-tree-visualizer-timestamps t)
  
  ;; Workaround for magit/persp bug, run this when you get "stringp error" when
  ;; doing SPC b b
  (with-eval-after-load "persp-mode"
    (defun persp-remove-killed-buffers ()
      (interactive)
      (mapc #'(lambda (p)
                (when p
                  (setf (persp-buffers p)
                        (delete-if-not #'buffer-live-p
                                       (persp-buffers p)))))
            (persp-persps)))
    )
    
  ;; I found that indent-guide can cause lots of slowdown in Org files, even if
  ;; they aren't huge (no long lines, <150k, <3000 lines). It can also be a bit
  ;; slow for other kinds of files if they're big enough, so let's try disabling
  ;; (spacemacs/toggle-indent-guide-globally-on)
  
  ;; Python shell config
  ;; (setq python-shell-completion-native nil)
  ;; (setq python-shell-interpreter "python.bat")   Special case for DHX
  
  ;;;; Nord colourscheme configs
  ;; Set visual-select/highlight colours
  (setq nord-region-highlight "snowstorm")
  ;; Make comments a little easier to read than the default
  (setq nord-comment-brightness 20)
  
  ;; Set cities for Wttr.in queries
  ;(setq wttrin-default-cities '("Tokyo"
  ;                              "Vancouver"))

  (custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   '(org-agenda-dimmed-todo-face ((t (:background "disabledControlTextColor")))))

  ;; Set tabs
  ;(setq javascript-indent-level 2)
  ;(setq js-indent-level 2)
  ;(setq js2-basic-offset 2) ; js2-mode, in latest js2-mode, it's alias of js-indent-level
  ;(setq web-mode-markup-indent-offset 2) ; web-mode, html tag in html file
  ;(setq web-mode-css-indent-offset 2) ; web-mode, css in html file
  ;(setq web-mode-scss-indent-offset 2)
  ;(setq web-mode-code-indent-offset 2) ; web-mode, js code in html file
  ;(setq css-indent-offset 2) ; css-mode
  ;(setq scss-indent-offset 2)

  ;; hack to fix py shell "not supporting readline" error
  ;(with-eval-after-load 'python
  ;  (defun python-shell-completion-native-try ()
  ;    "Return non-nil if can trigger native completion."
  ;    (let ((python-shell-completion-native-enable t)
  ;           (python-shell-completion-native-output-timeout
  ;            python-shell-completion-native-try-output-timeout))
  ;       (python-shell-completion-native-get-completions
  ;        (get-buffer-process (current-buffer))
  ;        nil "_"))))
  
  ;; Ledger configs
  ;(setq ledger-binary-path "F\\:/Programs/ledger/ledger.exe") ; Customize this
  ;(setq my/ledger-main-file "journal.dat")
  ;(setq ledger-reports
  ;  (backquote
  ;   (("bal" ,(concat ledger-binary-path " [[ledger-mode-flags]] -f " my/ledger-main-file " bal"))
  ;    ("reg" ,(concat ledger-binary-path " [[ledger-mode-flags]] -f " my/ledger-main-file " reg"))
  ;    ("bal-credit" ,(concat ledger-binary-path " [[ledger-mode-flags]] -f " my/ledger-main-file " bal credit"))
  ;    ("reg-credit" ,(concat ledger-binary-path " [[ledger-mode-flags]] -f " my/ledger-main-file " reg credit"))
  ;    ("reg-checking" ,(concat ledger-binary-path " [[ledger-mode-flags]] -f " my/ledger-main-file " reg checking"))
  ;    ("payee" ,(concat ledger-binary-path " [[ledger-mode-flags]] -f " my/ledger-main-file " reg @%(payee)"))
  ;    ("account" ,(concat ledger-binary-path " [[ledger-mode-flags]] -f " my/ledger-main-file " reg @%(account)")))))
  ;(setq ledger-post-amount-alignment-column 70)
  
  ;; Disable the fancy language server protocol ui stuff on this machine since
  ;; I' mainly just using JS and python, where the features don't really do much
  ;; due to lack of typing.
  ;(lsp-ui-mode nil)
  ;(setq lsp-ui-sideline-enable nil)

  ;; Use Ibuffer for Buffer List
  (global-set-key (kbd "C-x C-b") 'ibuffer)

  ;; Location of SASS lint config. Dont forget to `npm install -g sass-lint`
  (setq flycheck-sass-lintrc "~/.sasslintrc")
  
  ;; Make sure this is installed and not pointing to something weird. I've had issues with `nvm` in 
  ;; the past interfering with this and requiring it to be explicitly set.
  ;(setq flycheck-javascript-eslint-executable "/usr/local/bin/eslint")
  
  ;; Ranger config
  (setq ranger-enter-with-minus t)
  (setq ranger-cleanup-on-disable t)
  (setq ranger-show-hidden t)

  ;; Enable beacon-mode always (highlight cursor when it jumps)
  (beacon-mode 1)
  
  ;; Slack config. See https://github.com/yuya373/emacs-slack for instructions
  ;; on getting the client ID and token. The secret is the password. The name 
  ;; is the org name in the URL, i.e. XXX.slack.com
  ;(slack-register-team
  ; :name ""
  ; :default t
  ; :client-id ""
  ; :client-secret ""
  ; :token ""
  ; :subscribed-channels '(general))
   
  ;; On OSX, try to fix the weird inconsistencies between the system path and emacs' path.
  ;(when (memq window-system '(mac ns x))
  ;  (exec-path-from-shell-initialize))
    
  ;; Speed up default Avy timeout
  (setq avy-timeout-seconds 0.2)
  
  ;; Let's try putting common functions on the normally-useless s/S keys
  (define-key evil-normal-state-map (kbd "s") 'avy-goto-char-timer)
  (define-key evil-normal-state-map (kbd "S") 'frog-jump-buffer)
  
  ;; For some reason YAS stopped turning on automatically, so let's force it
  ;; like this.
  ;(add-hook 'jinja2-mode-hook 'yas-minor-mode)

  ;; This almost works, but not quite...opening the shell seems to have weird
  ;; inconsistent behavior WRT which buffer if feels like using, or whether it
  ;; just creates a new buffer.
  ;(spacemacs|define-custom-layout "@waymo-shells"
  ;  :binding "w"
  ;  :body
  ; (let ((default-directory "~/Documents/google-waymo"))
  ;    (split-window-right)
  ;    (winum-select-window-2)
  ;    (shell "waymo-site-shell"))
  ;  (let ((default-directory "~/Documents/waymo-brand"))
  ;    (winum-select-window-2)
  ;    (shell "branding-shell")
  ;    (split-window-below-and-focus))
  ;  (let ((default-directory "~/Documents/grow-blogger"))
  ;    (shell "blog-shell")))
  
  ;; Use dumb-jump to replace the default evil "gd" behaviour since it works
  ;; more consistently.
  ;;
  ;; Sometimes dumb-jump hits a problem where it tries to use
  ;; a Helm function, but if Helm has been updated/recompiled since the last
  ;; time dumb-jump was compiled, it fails. See
  ;; https://github.com/syl20bnr/spacemacs/issues/12096.
  ;;
  ;; Fix:
  ;; 1) Delete the compiled dumb-jump file. Probably at something like:
  ;;    ~/.emacs.d/elpa/26.2/develop/dumb-jump-20190907.450/dumb-jump.elc
  ;; 2) Run `spacemacs/recompile-elpa`
  ;; 3) Restart Emacs
  (define-key evil-normal-state-map (kbd "gd") 'dumb-jump-go)
  
  ;; Ligature setup
  (let ((alist '((33 . ".\\(?:\\(?:==\\|!!\\)\\|[!=]\\)")
                 (35 . ".\\(?:###\\|##\\|_(\\|[#(?[_{]\\)")
                 (36 . ".\\(?:>\\)")
                 (37 . ".\\(?:\\(?:%%\\)\\|%\\)")
                 (38 . ".\\(?:\\(?:&&\\)\\|&\\)")
                 (42 . ".\\(?:\\(?:\\*\\*/\\)\\|\\(?:\\*[*/]\\)\\|[*/>]\\)")
                 (43 . ".\\(?:\\(?:\\+\\+\\)\\|[+>]\\)")
                 (45 . ".\\(?:\\(?:-[>-]\\|<<\\|>>\\)\\|[<>}~-]\\)")
                 (46 . ".\\(?:\\(?:\\.[.<]\\)\\|[.=-]\\)")
                 (47 . ".\\(?:\\(?:\\*\\*\\|//\\|==\\)\\|[*/=>]\\)")
                 (48 . ".\\(?:x[a-zA-Z]\\)")
                 (58 . ".\\(?:::\\|[:=]\\)")
                 (59 . ".\\(?:;;\\|;\\)")
                 (60 . ".\\(?:\\(?:!--\\)\\|\\(?:~~\\|->\\|\\$>\\|\\*>\\|\\+>\\|--\\|<[<=-]\\|=[<=>]\\||>\\)\\|[*$+~/<=>|-]\\)")
                 (61 . ".\\(?:\\(?:/=\\|:=\\|<<\\|=[=>]\\|>>\\)\\|[<=>~]\\)")
                 (62 . ".\\(?:\\(?:=>\\|>[=>-]\\)\\|[=>-]\\)")
                 (63 . ".\\(?:\\(\\?\\?\\)\\|[:=?]\\)")
                 (91 . ".\\(?:]\\)")
                 (92 . ".\\(?:\\(?:\\\\\\\\\\)\\|\\\\\\)")
                 (94 . ".\\(?:=\\)")
                 (119 . ".\\(?:ww\\)")
                 (123 . ".\\(?:-\\)")
                 (124 . ".\\(?:\\(?:|[=|]\\)\\|[=>|]\\)")
                 (126 . ".\\(?:~>\\|~~\\|[>=@~-]\\)")
                 )
               ))
    (dolist (char-regexp alist)
      (set-char-table-range composition-function-table (car char-regexp)
                            `([,(cdr char-regexp) 0 font-shape-gstring]))))
  
  ;;;;;; Ivy Config ;;;;;;;
  
  ;; If Helm ever shits itself, remove the 'helm' layer above and add 'ivy' 
  ;; and it will take care of remapping all the basic commands to Ivy automatically.
  (setq ivy-re-builders-alist
        '((read-file-name-internal . ivy--regex-fuzzy)
          (counsel-projectile-find-file . ivy--regex-fuzzy)
          (counsel-recentf . ivy--regex-fuzzy)
          (counsel-switch-buffer . ivy--regex-fuzzy)
          (counsel-buffer-or-recentf . ivy--regex-fuzzy)
          (counsel-M-x . ivy--regex-fuzzy)
          (counsel-apropos . ivy--regex-fuzzy)
          (counsel-describe-function . ivy--regex-fuzzy)
          (counsel-describe-face . ivy--regex-fuzzy)
          (counsel-describe-variable . ivy--regex-fuzzy)
          (counsel-imenu . ivy--regex-fuzzy)
          (counsel-org-goto . ivy--regex-fuzzy)
          ;(t . ivy--regex-plus))) Not sure which of these I like better yet.
          (t . ivy--regex-ignore-order)))
          
  ;; Make `SPC b b' more helpful by also including recentf. This is more similar
  ;; to what was bound to that keybinding while Helm was being used.
  (spacemacs/set-leader-keys "b b" 'counsel-buffer-or-recentf)
  
  ;; Use this instead of `SPC /'. It's basically the same, but for some reason,
  ;; you can't apply the universal argument to `SPC /', probably because there's
  ;; like an indirect function in between which loses the argument. So get in
  ;; the habit of doing this instead. The Universal Arugment here (i.e. `SPC u
  ;; SPC p s') allows you to supply arguments to ripgrep in the minibuffer,
  ;; before entering the search time. The main use of this is to narrow the
  ;; search down to a specific file type, by using args like `-t html'.
  (spacemacs/set-leader-keys "p s" 'counsel-projectile-rg)

  ; TODO: Why isn't this working?
  ;; To use a writeable ivy/counsel session, you have to do C-x C-o (ivy-occur)
  ;; and then C-c C-p (ivy-wgrep-change-to-wgrep-mode), then C-c C-e to finish
  ;; writing. Let's try to make this slightly more consistent with Dired/Ranger
  ;(spacemacs/set-leader-keys-for-major-mode 'ivy-occur-mode "C-c C-e" 'ivy-wgrep-change-to-wgrep-mode)
  
  ;;;;;; End Ivy config ;;;;;;;
  
  ;; Remove some stuff I never look at from the mode line
  (spacemacs/toggle-mode-line-minor-modes-off)
  (spacemacs/toggle-mode-line-version-control-off)

  )

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.

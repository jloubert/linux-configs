"" Plugins
call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'

" Start screen
Plug 'mhinz/vim-startify'

" Completion
Plug 'ajh17/VimCompletesMe'

" Colours
Plug 'morhetz/gruvbox'

" Fuzzy finder
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-rooter'

call plug#end()


"" Basic Configs
inoremap kj <esc>

" Line numbers
set number
set relativenumber
if has('autocmd')
	augroup vimrc_linenumbering
		" Set line numbers to relative for the current window only
		autocmd!
		autocmd WinLeave *
					\ if &number |
					\   set norelativenumber |
					\ endif
		autocmd BufWinEnter *
					\ if &number |
					\   set relativenumber |
					\ endif
		autocmd VimEnter *
					\ if &number |
					\   set relativenumber |
					\ endif
	augroup END
endif

set smartcase

"Open files to same position as last time
augroup resCur
	autocmd!
	autocmd BufReadPost * call setpos(".", getpos("'\""))
augroup END

" Shortcut "w!!" for forcing vim to write with sudo permissions
cmap w!! w !sudo tee %

" Set indentation to 4-spaces
set tabstop=4 expandtab shiftwidth=4 smarttab

set encoding=utf8

" Go to next/prev buffers with gn/gp
nmap gn :bn<CR>
nmap gp :bp<CR>
nmap gd :bd<CR>

" Open new splits to the right and bottom
set splitbelow
set splitright

" Push F3 to insert a timestamp
nmap <F3> i<C-R>=strftime("[%Y-%m-%d %a %I:%M %p]")<CR><Esc>
imap <F3> <C-R>=strftime("[%Y-%m-%d %a %I:%M %p]")<CR>

" Trigger a highlight (from quick-scope plugin) in the appropriate direction when pressing these keys:
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

" Make Y yank everything from the cursor to the end of the line. This makes Y
" act more like C or D because by default, Y yanks the current line (i.e. the
" same as yy).
noremap Y y$

" Make Ctrl-e jump to the end of the current line in the insert mode. This is
" handy when you are in the middle of a line and would like to go to its end
" without switching to the normal mode.
inoremap <C-e> <C-o>$

" Let the Startify plugin automatically update sessions
let g:startify_session_persistence = 1

" Clean up buffers before loading a session
let g:startify_session_delete_buffers = 1

" Allow macros to be executed on every line in a visual selection
xnoremap @ :<C-u>call ExecuteMacroOverVisualRange()<CR>
function! ExecuteMacroOverVisualRange()
  echo "@".getcmdline()
  execute ":'<,'>normal @".nr2char(getchar())
endfunction

" Select what you just pasted (must use right after you paste it)
nnoremap <expr> gV    "`[".getregtype(v:register)[0]."`]"

" Set the colourscheme
colorscheme gruvbox
set background=dark

" FZF config
nnoremap <C-p> :Files<CR>


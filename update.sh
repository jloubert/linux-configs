#! /bin/zsh

RED=`tput setaf 1`
GREEN=`tput setaf 2`
RESET=`tput sgr0`

declare -a PATHS=("$HOME/" "$HOME/.config/i3/")

echo "Syncing dotfiles to local user..."

git pull

for FILE in $(git ls-tree -r master --name-only)
do
    FOUND_FILE=false

    for FILE_PATH in "${PATHS[@]}"
    do
        FILE_NAME=$FILE_PATH$FILE
        if [ -f "$FILE_NAME" ]; then
            FOUND_FILE=true
            echo "${GREEN}$(cp -v $FILE $FILE_PATH)${RESET}"
            break
        fi
    done


    if [ $FOUND_FILE = false ]; then
        echo "${RED}$FILE not found.${RESET}"
    fi
done

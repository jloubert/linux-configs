# Path to your oh-my-zsh installation.
#export ZSH=/home/jon/.oh-my-zsh

# If you come from bash you might have to change your $PATH.
#export PATH=$HOME/bin:/usr/local/bin:$PATH

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.

# Fancy theme with powerline chars:
#ZSH_THEME="agnoster"


# alternative with no fancy chars:
ZSH_THEME="rkj-repos"
#ZSH_THEME="amuse"


# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git zsh-autosuggestions npm sudo colored-man-pages colorize cp django lein node pip systemd vim-interaction zsh-nvm zsh-syntax-highlighting)
# Don't forget..."zsh-syntax-highlighting" MUST**** be last!!
#plugins=(git colored-man-pages colorize cp django lein node npm pip sudo systemd thefuckubuntu vim-interaction)

source $ZSH/oh-my-zsh.sh

# User configuration

# export PATH="/home/jon/Applications/.bin:/usr/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl"
# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
#export LANG=en_US.utf8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='vim' # TODO: change back to NVIM?
fi

export VISUAL='vim' # TODO: change back to NVIM?

# Compilation flags
#export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
alias zshconfig="sudo vim ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# VBoxClient-all
#export LC_ALL=en_US.utf8

# Don't put these commands into the shell history
HISTIGNORE='fg:history'

# Ignore blank commands and duplicates in shell
# history
HISTCONTROL=ignoreboth:erasedups

# Append commands to the shell history as you
# exexecute them, instead of only when a shell
# session terminates. This preserves history even
# when the shell crashes etc
PROMPT_COMMAND='history -a'

source $ZSH_HIGHLIGHT

# eval $(thefuck --alias)

# RIP tar flags
extract() {
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2) tar xvjf $1;;
            *.tar.gz) tar xvzf $1;;
            *.bz2) bunzip2 $1;;
            *.rar) unrar x $1;;
            *.gz) gunzip $1;;
            *.tar) tar xvf $1;;
            *.tbz2) tar xvjf $1;;
            *.tgz) tar xvzf $1;;
            *.zip) unzip $1;;
            *.Z) uncompress $1;;
            *.7z) 7za x $1;;
            *.rar) unrar $1;;
            *) echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}


### CUSTOM PROMPT
# Source: https://github.com/Parth/dotfiles/blob/master/zsh/prompt.sh
# Reference for colors: http://stackoverflow.com/questions/689765/how-can-i-change-the-color-of-my-prompt-in-zsh-different-from-normal-text

autoload -U colors && colors

setopt PROMPT_SUBST

set_prompt() {

        # [
        PS1="%{$fg[white]%}[%{$reset_color%}"

        # Path: http://stevelosh.com/blog/2010/02/my-extravagant-zsh-prompt/
        PS1+="%{$fg_bold[cyan]%}${PWD/#$HOME/~}%{$reset_color%}"

        # Status Code
        PS1+='%(?.., %{$fg[red]%}%?%{$reset_color%})'

        # Git
        if git rev-parse --is-inside-work-tree 2> /dev/null | grep -q 'true' ; then
                PS1+=', '
                PS1+="%{$fg[blue]%}$(git rev-parse --abbrev-ref HEAD 2> /dev/null)%{$reset_color%}"
                if [ $(git status --short | wc -l) -gt 0 ]; then
                        PS1+="%{$fg[red]%}+$(git status --short | wc -l | awk '{$1=$1};1')%{$reset_color%}"
                fi
        fi

        # Timer: http://stackoverflow.com/questions/2704635/is-there-a-way-to-find-the-running-time-of-the-last-executed-command-in-the-shel
        if [[ $_elapsed[-1] -ne 0 ]]; then
                PS1+=', '
                PS1+="%{$fg[magenta]%}$_elapsed[-1]s%{$reset_color%}"
        fi

        # PID
        if [[ $! -ne 0 ]]; then
                PS1+=', '
                PS1+="%{$fg[yellow]%}PID:$!%{$reset_color%}"
        fi

        # Sudo: https://superuser.com/questions/195781/sudo-is-there-a-command-to-check-if-i-have-sudo-and-or-how-much-time-is-left
        CAN_I_RUN_SUDO=$(sudo -n uptime 2>&1|grep "load"|wc -l)
        if [ ${CAN_I_RUN_SUDO} -gt 0 ]
        then
                PS1+=', '
                PS1+="%{$fg_bold[red]%}SUDO%{$reset_color%}"
        fi

    # Timestamp
    PS1+=", %D{%T}"

    # Hostname
    if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ] || [ -n "$SSH_CONNECTION" ]; then
        PS1+=", %{$fg[blue]%}%M"
    fi

    # Prompt on new line
    PS1+="%{$fg[white]%}]"
    PS1+=$'\n$ %{$reset_color%}% '
}

precmd_functions+=set_prompt

preexec () {
   (( ${#_elapsed[@]} > 1000 )) && _elapsed=(${_elapsed[@]: -1000})
   _start=$SECONDS
}

precmd () {
   (( _start >= 0 )) && _elapsed+=($(( SECONDS-_start )))
   _start=-1
}

# This helps to fix Emacs on MacOS.
#
# Try installing `emacs-plus` on Homebrew. If the install complains
# about libgccjit being missing (even though you already
# brew-installed that), try
#
# set LIBRARY_PATH="/usr/local/Cellar/libgccjit/11.2.0_1"
#
# or to wherever the main libgccjit folder is, before doing the brew
# install for Emacs. You can probably find the folder by doing `locate
# libgccjit` and seeing which folder all the stuff is in.
#
# Once past the initial Emacs install (which may take 10+ minutes),
# try running this to fix future Emacs runs so that it can find the
# libgccjit compiler whenever it tries to compile libraries. This
# means you will have to restart Emacs now, if you're already inside
# Emacs as you read this.
#
# I got this from
# https://github.com/d12frosted/homebrew-emacs-plus/issues/378.

alias pbd=/usr/libexec/PlistBuddy
export emacsapp=`brew --prefix --installed emacs-plus@28`/Emacs.app
export infoplist="${emacsapp}/Contents/Info.plist"

pbd -c "Add :LSEnvironment dict" "${infoplist}"
pbd -c "Add :LSEnvironment:PATH string" "${infoplist}"
pbd -c "Set :LSEnvironment:PATH $(echo "$PATH")" "${infoplist}"
pbd -c "Print :LSEnvironment" "${infoplist}"
touch ${emacsapp}

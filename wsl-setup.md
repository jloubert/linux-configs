# WSL Setup Notes #

Some rough notes while I try to set up Ubuntu on Windows, so I can try to reproduce
all this stuff again some time. 

1. Install wsltty?
    - Not sure about this, let's revisit. I don't get what benefit this gives, but
      lots of people recommend it.
2. Install Cmder
3. Create a new Cmder action: Ubuntu, and set the command to just Ubuntu
    - This is assuming no wsltty.
4. Install zsh from apt repos.
5. Install oh-my-zsh and <https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md>
   by following instructions on their github pages.
6. Update Ubuntu with apt upgrade && apt update
7. Grab dotfiles from this repo (vim, gitignore, bash/zsh, emacs, etc)

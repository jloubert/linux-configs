#!/bin/bash

# Try to guess the package manager
if apt -v ; then
	echo "Using apt..."
	INSTALL="apt install"
elif pacman -V ; then
	echo "Using pacman..."
	INSTALL="pacman -S"
else
	echo "Could not guess package manager!"
	exit 1
fi


## Apps

# Basics
$INSTALL git neovim zsh tmux curl dos2unix emacs
# Others
$INSTALL xsel ranger htop mlocate


## TMUX

# Deploy config file
cp -v -i ~/dotfiles/deploy/tmux.conf ~

# tmux-yank to clipboard plugin. Use "y" in highlight mode (`C-\ [`)
mkdir ~/dotfiles/tmux
git clone https://github.com/tmux-plugins/tmux-yank ~/dotfiles/tmux/tmux-yank


## ZSH

# Install Oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Install zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
echo "source ${(q-)PWD}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> ${ZDOTDIR:-$HOME}/.zshrc
cp -v -i ~/dotfiles/deploy/.zshrc ~

# Brute-force all the line endings?? There must be a better way.
# find ~/ -type f -print0 | xargs -0 -n 1 -P 4 dos2unix

chsh -s /usr/bin/zsh


## VIM

# Get Vim-plug
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Deploy the config file. Need to run `:PlugInstall` command in nvim afterward
ln -s -v -i init.vim ~/.config/nvim/init.vim


## Emacs

ln -s -v -i ./.spacemacs ~/.spacemacs


## Misc other configs

# Git
ln -s -v -i ./.gitconfig ~/.gitconfig
ln -s -v -i ./.global_gitignore ~/.global_gitignore

# i3 status bar
ln -s -v -i ./.i3status.conf ~/.i3status.conf

